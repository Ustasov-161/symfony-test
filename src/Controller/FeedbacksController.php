<?php

namespace App\Controller;

use App\Form\FeedbacksType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Feedbacks;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeedbacksController extends AbstractController
{

    /**
     * @return Response
     * @Route("/form", name="form")
     */
    public function new()
    {


        // creates a task object and initializes some data for this example
        $feedbacks = new Feedbacks();

        $form = $this->createForm(FeedbacksType::class, $feedbacks);

        return $this->render('feedbacks/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }


}
